package br.com.dev.gestaousuarioteste.core.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void equalsTest() {
        assertEquals(generateUser(), generateUser());
    }

    @Test
    void hashCodeTest() {
        assertEquals(generateUser().hashCode(), generateUser().hashCode());
    }

    @Test
    void toStringTest() {
        assertEquals(generateUser().toString(), generateUser().toString());
    }

    private User generateUser(){
        User user = new User();
        user.setId(1L);
        user.setUsername("teste1");
        user.setPassword("1233412");
        user.setEmail("teste1@gmail");
        return user;
    }
}