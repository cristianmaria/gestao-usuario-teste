package br.com.dev.gestaousuarioteste.core.mapper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserMapperTest {

    @Test
    void toUserTest_null() {
        Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            UserMapper.toUserEntity(null);
        });
        assertEquals("Objeto user nao pode ser nulo", exception.getMessage());
    }

    @Test
    void toUserEntityTest_null() {
        Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            UserMapper.toUser(null);
        });
        assertEquals("Objeto userEntity nao pode ser nulo", exception.getMessage());
    }
}