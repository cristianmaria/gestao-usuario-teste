package br.com.dev.gestaousuarioteste.core.domain.validate;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.core.exceptions.BusinessRuleException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateUserTest {

    @Test
    void validateTest_null() {
        Exception exception =  assertThrows(BusinessRuleException.class, () -> {
            ValidateUser.validate(null);
        });
        assertEquals("Objeto user deve pode ser nulo", exception.getMessage());
    }

    @Test
    void validateTest_invalid_username() {
        User user = new User();
        Exception exception =  assertThrows(BusinessRuleException.class, () -> {
            ValidateUser.validate(user);
        });
        assertEquals("Parametro userName deve ser informado", exception.getMessage());

        user.setUsername("");
        exception =  assertThrows(BusinessRuleException.class, () -> {
            ValidateUser.validate(user);
        });
        assertEquals("Parametro userName deve ser informado", exception.getMessage());
    }

    @Test
    void validateTest_invalid_passsword() {
        User user = new User();
        user.setUsername("joao");
        Exception exception =  assertThrows(BusinessRuleException.class, () -> {
            ValidateUser.validate(user);
        });
        assertEquals("Parametro password deve ser informado", exception.getMessage());

        user.setUsername("joao");
        user.setPassword("");
        exception =  assertThrows(BusinessRuleException.class, () -> {
            ValidateUser.validate(user);
        });
        assertEquals("Parametro password deve ser informado", exception.getMessage());
    }

    @Test
    void validateTest_invalid_email() {
        User user = new User();
        user.setUsername("joao");
        user.setPassword("1234567");
        Exception exception =  assertThrows(BusinessRuleException.class, () -> {
            ValidateUser.validate(user);
        });
        assertEquals("Parametro email deve ser informado", exception.getMessage());

        user.setUsername("joao");
        user.setPassword("1234567");
        user.setEmail("");
        exception =  assertThrows(BusinessRuleException.class, () -> {
            ValidateUser.validate(user);
        });
        assertEquals("Parametro email deve ser informado", exception.getMessage());
    }

}