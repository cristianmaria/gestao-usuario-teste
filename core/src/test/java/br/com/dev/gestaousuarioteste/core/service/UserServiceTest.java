package br.com.dev.gestaousuarioteste.core.service;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.core.exceptions.BusinessRuleException;
import br.com.dev.gestaousuarioteste.core.mapper.UserMapper;
import br.com.dev.gestaousuarioteste.model.dao.UserDAO;
import static org.junit.jupiter.api.Assertions.*;

import br.com.dev.gestaousuarioteste.model.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;


@RunWith(MockitoJUnitRunner.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserDAO userDAO;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveUserTest_sucess() {
        User user = generateUser1();
        UserEntity userEntity = generateUserEntity1();
        given(this.userDAO.saveUser(userEntity)).willReturn(userEntity);

        User result = userService.saveUser(user);
        assertNotNull(result);
        assertEquals(result, user);

    }

    @Test
    void saveUserTest_error() {
        User user = new User();

        Exception exception =  assertThrows(BusinessRuleException.class, () -> {
            userService.saveUser(user);
        });

        assertEquals("Parametro userName deve ser informado", exception.getMessage());

    }

    @Test
    void listUsersTest() {
        List<UserEntity> usersEntity = new ArrayList<>();
        usersEntity.add(generateUserEntity1());
        usersEntity.add(generateUserEntity2());

        given(this.userDAO.listUsers()).willReturn(usersEntity);

        List<User> usersResult = userService.listUsers();

        assertEquals(generateUser1(), usersResult.get(0));
        assertEquals(generateUser2(), usersResult.get(1));

    }

    @Test
    void updateUserTest_sucess() {

        given(this.userDAO.updateUser(generateUserEntity1())).willReturn(generateUserEntity1());
        given(this.userDAO.isUserCad(1L)).willReturn(true);

        User result = userService.updateUser(generateUser1());
        assertNotNull(result);
        assertEquals(result, generateUser1());
    }

    @Test
    void updateUserTest_error() {

        given(this.userDAO.updateUser(generateUserEntity2())).willReturn(generateUserEntity2());
        given(this.userDAO.isUserCad(2L)).willReturn(false);

        Exception exception =  assertThrows(BusinessRuleException.class, () -> {
            userService.updateUser(generateUser2());
        });

        assertEquals("Usuario nao cadastrado", exception.getMessage());

    }

    private User generateUser1(){
        User user = new User();
        user.setId(1L);
        user.setUsername("teste1");
        user.setPassword("1233412");
        user.setEmail("teste1@gmail");
        return user;
    }

    private User generateUser2(){
        User user = new User();
        user.setId(2L);
        user.setUsername("teste2");
        user.setPassword("87565432");
        user.setEmail("teste2@gmail");
        return user;
    }

    private UserEntity generateUserEntity1(){
       return UserMapper.toUserEntity(generateUser1());
    }

    private UserEntity generateUserEntity2(){
        return UserMapper.toUserEntity(generateUser2());
    }
}