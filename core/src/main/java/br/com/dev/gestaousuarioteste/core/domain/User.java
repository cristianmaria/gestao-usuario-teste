package br.com.dev.gestaousuarioteste.core.domain;

import lombok.*;

@Data
@Builder@NoArgsConstructor@AllArgsConstructor
@Generated
public class User {

    private Long id;
    private String username;
    private String password;
    private String email;

}
