package br.com.dev.gestaousuarioteste.core.domain.validate;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.core.exceptions.BusinessRuleException;

public class ValidateUser {

    private ValidateUser(){}

    public static void validate(User user) throws BusinessRuleException {

        if (user == null) {
            throw new BusinessRuleException("Objeto user deve pode ser nulo");
        } else if (user.getUsername() == null || "".equals(user.getUsername()) ) {
            throw new BusinessRuleException("Parametro userName deve ser informado");
        } else if (user.getPassword() == null || "".equals(user.getPassword()) ) {
            throw new BusinessRuleException("Parametro password deve ser informado");
        } else if (user.getEmail() == null || "".equals(user.getEmail()) ) {
            throw new BusinessRuleException("Parametro email deve ser informado");
        }
    }
}
