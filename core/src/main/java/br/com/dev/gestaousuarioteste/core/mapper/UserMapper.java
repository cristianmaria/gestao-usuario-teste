package br.com.dev.gestaousuarioteste.core.mapper;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.model.entity.UserEntity;

public class UserMapper {

    private UserMapper (){}

    public static User toUser(UserEntity userEntity) {
        if (userEntity == null) {
            throw new IllegalArgumentException("Objeto userEntity nao pode ser nulo");
        }

        return User.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .password(userEntity.getPassword())
                .email(userEntity.getEmail())
                .build();
    }

    public static UserEntity toUserEntity(User user) {
        if (user == null) {
            throw new IllegalArgumentException("Objeto user nao pode ser nulo");
        }

        return UserEntity.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .email(user.getEmail())
                .build();
    }

}
