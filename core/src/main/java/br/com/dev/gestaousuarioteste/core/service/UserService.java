package br.com.dev.gestaousuarioteste.core.service;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.core.domain.validate.ValidateUser;
import br.com.dev.gestaousuarioteste.core.exceptions.BusinessRuleException;
import br.com.dev.gestaousuarioteste.core.mapper.UserMapper;
import br.com.dev.gestaousuarioteste.model.dao.UserDAO;
import br.com.dev.gestaousuarioteste.model.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public User saveUser(User user) {
        log.info("Cadastrando usuario {}",user.getUsername());
        ValidateUser.validate(user);
        UserEntity userEntity = userDAO.saveUser(UserMapper.toUserEntity(user));
        return UserMapper.toUser(userEntity);
    }

    public List<User> listUsers() {
        log.info("Serviço de listagem de usuario");
        List<UserEntity> userEntityList = userDAO.listUsers();

        return userEntityList.stream()
                .map(item -> UserMapper.toUser(item))
                .collect(Collectors.toList());
    }

    public User updateUser(User user) {
        ValidateUser.validate(user);
        log.info("Verificando se o usuario existe na base");
        if (!userDAO.isUserCad(user.getId())) {
            log.info("Usuario nao cadastrado");
            throw new BusinessRuleException("Usuario nao cadastrado");
        }

        log.info("Atualizando usuario {}",user.getUsername());
        UserEntity userEntity = userDAO.updateUser(UserMapper.toUserEntity(user));
        return UserMapper.toUser(userEntity);
    }

}
