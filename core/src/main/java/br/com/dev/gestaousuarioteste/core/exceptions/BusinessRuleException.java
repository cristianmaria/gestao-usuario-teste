package br.com.dev.gestaousuarioteste.core.exceptions;

public class BusinessRuleException extends RuntimeException{

    public BusinessRuleException(String errorMessage) {
        super(errorMessage);
    }

}
