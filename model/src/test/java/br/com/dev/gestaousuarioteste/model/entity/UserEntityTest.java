package br.com.dev.gestaousuarioteste.model.entity;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserEntityTest {

    @Test
    void equalsTest() {
        //mesmo objeto
        Assert.assertTrue(generateUserBuilder().equals(generateUserBuilder()));
        //objeto diferente
        Assert.assertNotEquals(generateUserBuilder(), generateUserBuilder2());

        Assert.assertEquals(new UserEntity(), new UserEntity());
        Assert.assertTrue(new UserEntity().equals(new UserEntity()));
    }

    @Test
    void hashCodeTest() {
        //mesmo objeto
        Assert.assertEquals(generateUserBuilder().hashCode(), generateUserBuilder().hashCode());
        //objeto diferente
        Assert.assertNotEquals(generateUserBuilder().hashCode(), generateUserBuilder2().hashCode());
        //Para cada objeto criado pelo builder, o hash deve ser diferente
        Assert.assertNotEquals(UserEntity.builder().hashCode(), UserEntity.builder().hashCode());

    }

    @Test
    void builderTest() {

        //mesmo objeto
        Assert.assertEquals(generateUserBuilder(), generateUserBuilder());
        //objeto diferente
        Assert.assertNotEquals(generateUserBuilder(), generateUserBuilder2());
    }

    @Test
    void toStringTest() {

        Assert.assertEquals(UserEntity.builder().toString(),UserEntity.builder().toString());

    }

    private UserEntity generateUserBuilder() {
        UserEntity user1 = UserEntity.builder()
                .id(1l)
                .username("teste1")
                .password("1233412")
                .email("teste1@gmail").build();
        return user1;
    }

    private UserEntity generateUserBuilder2(){
        UserEntity user1 = UserEntity.builder()
                .id(2l)
                .username("teste2")
                .password("98765543221")
                .email("teste2@gmail").build();
        return user1;
    }

}