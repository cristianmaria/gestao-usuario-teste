package br.com.dev.gestaousuarioteste.model.dao;

import br.com.dev.gestaousuarioteste.model.entity.UserEntity;
import br.com.dev.gestaousuarioteste.model.repository.IUserRepository;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
class UserDAOTest {

    @InjectMocks
    private UserDAO userDAO;

    @Mock
    private IUserRepository userRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void saveUser() {
        UserEntity user1 = generateUser1();
        given(this.userRepository.save(user1)).willReturn(user1);
        UserEntity userResult = userDAO.saveUser(user1);
        assertEquals(user1, userResult);
    }

    @Test
    public void listUsers() {
        List<UserEntity> users = new ArrayList<>();
        users.add(generateUser1());
        users.add(generateUser2());
        given(this.userRepository.findAll()).willReturn(users);
        List<UserEntity> usersResult = this.userDAO.listUsers();
        assertEquals(users.get(0), usersResult.get(0));
        assertEquals(users.get(1), usersResult.get(1));
    }

    @Test
    public void updateUser() {
        UserEntity user1 = generateUser1();
        given(this.userRepository.save(user1)).willReturn(user1);
        UserEntity userResult = userDAO.updateUser(user1);

        assertEquals(user1,userResult );
        assertEquals(user1.toString(),userResult.toString() );
        assertEquals(user1.toString(), userResult.toString());
    }

    @Test
    public void isUserCad() {
        UserEntity user1 = generateUser1();
        Long id = 1L;
        given(this.userRepository.findById(id)).willReturn(Optional.of(user1));

        assertTrue(userDAO.isUserCad(id));
        assertFalse(userDAO.isUserCad(2L));
    }

    private UserEntity generateUser1(){
        UserEntity user1 = new UserEntity();
        user1.setId(1L);
        user1.setUsername("teste1");
        user1.setPassword("1233412");
        user1.setEmail("teste1@gmail");
        return user1;
    }

    private UserEntity generateUser2(){
        return UserEntity.builder()
                .id(2L)
                .username("teste2")
                .password("98765543221")
                .email("teste2@gmail").build();
    }
}