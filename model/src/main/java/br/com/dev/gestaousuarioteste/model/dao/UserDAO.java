package br.com.dev.gestaousuarioteste.model.dao;

import br.com.dev.gestaousuarioteste.model.entity.UserEntity;
import br.com.dev.gestaousuarioteste.model.repository.IUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Slf4j
public class UserDAO {

    @Autowired
    private IUserRepository userRepository;

    public UserEntity saveUser(UserEntity user){
        log.info("Salvando novo usuario");
        return userRepository.save(user);
    }

    public List<UserEntity> listUsers(){
        log.info("Busca lista usuario");
        return userRepository.findAll();
    }

    public UserEntity updateUser(UserEntity user){
        log.info("Atualizando usuario");
        return userRepository.save(user);
    }

    public boolean isUserCad(Long id){
        log.info("Verificando usuario cadastrados");
        Optional<UserEntity> userEntity = userRepository.findById(id);
        return !userEntity.isEmpty();
    }

}
