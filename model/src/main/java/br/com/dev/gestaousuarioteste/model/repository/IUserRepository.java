package br.com.dev.gestaousuarioteste.model.repository;

import br.com.dev.gestaousuarioteste.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<UserEntity, Long> {
}
