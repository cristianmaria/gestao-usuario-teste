package br.com.dev.gestaousuarioteste.model.entity;

import lombok.*;

import javax.persistence.*;

@Entity(name = "user")
@Data
@Builder
@NoArgsConstructor@AllArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String email;

}
