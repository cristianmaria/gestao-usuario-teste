package br.com.dev.gestaousuarioteste.rest.controller;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.core.service.UserService;
import br.com.dev.gestaousuarioteste.rest.dto.UserCadDTO;
import br.com.dev.gestaousuarioteste.rest.dto.UserDTO;
import br.com.dev.gestaousuarioteste.rest.mapper.UserCadDTOMapper;
import br.com.dev.gestaousuarioteste.rest.mapper.UserDTOMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/usuario")
@RestController@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/cadastrar", produces = "application/json")
    public UserDTO saveUser(@RequestBody UserCadDTO userCadDTO) {
        log.info("Salvando usuario");
        User user = userService.saveUser(UserCadDTOMapper.toUser(userCadDTO));
        return  UserDTOMapper.toUserDTO(user);
    }

    @GetMapping(value = "/listar", produces = "application/json")
    public List<UserDTO>  getListUsers() {
        log.info("Gerando lista de usuario");
        List<User> users = userService.listUsers();
        return UserDTOMapper.toListUserDTO(users);
    }

    @PostMapping(value = "/atualiza", produces = "application/json")
    public UserDTO updateUser(@RequestBody UserDTO userDTO) {
        log.info("Atualizando usuario");
        User user = userService.updateUser(UserDTOMapper.toUser(userDTO));
        return UserDTOMapper.toUserDTO(user);
    }

}
