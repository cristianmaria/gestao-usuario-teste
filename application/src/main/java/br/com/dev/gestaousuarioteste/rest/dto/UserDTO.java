package br.com.dev.gestaousuarioteste.rest.dto;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Generated
public class UserDTO {

    private Long id;
    private String username;
    private String password;
    private String email;

}
