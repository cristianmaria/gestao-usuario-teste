package br.com.dev.gestaousuarioteste.rest.mapper;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.rest.dto.UserCadDTO;

public class UserCadDTOMapper {

    private UserCadDTOMapper(){}

    public static User toUser(UserCadDTO userCadDTO) {
        if (userCadDTO == null) {
            throw new IllegalArgumentException("Objeto userCadDTO nao pode ser nulo");
        }

        return User.builder()
                .username(userCadDTO.getUsername())
                .password(userCadDTO.getPassword())
                .email(userCadDTO.getEmail())
                .build();
    }

}
