package br.com.dev.gestaousuarioteste.rest.mapper;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.rest.dto.UserDTO;

import java.util.List;
import java.util.stream.Collectors;

public class UserDTOMapper {

    private UserDTOMapper(){}

    public static User toUser(UserDTO userDTO) {
        if (userDTO == null) {
            throw new IllegalArgumentException("Objeto userDTO nao pode ser nulo");
        }
        return User.builder()
                .id(userDTO.getId())
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .email(userDTO.getEmail())
                .build();
    }

    public static UserDTO toUserDTO(User user) {
        if (user == null) {
            throw new IllegalArgumentException("Objeto user nao pode ser nulo");
        }

        return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .email(user.getEmail())
                .build();
    }

    public static List<UserDTO> toListUserDTO(List<User> users) {
        if (users == null) {
            throw new IllegalArgumentException("Objeto lista de user nao pode ser nulo");
        }

        return users.stream()
                .map(item -> toUserDTO(item))
                .collect(Collectors.toList());
    }
}
