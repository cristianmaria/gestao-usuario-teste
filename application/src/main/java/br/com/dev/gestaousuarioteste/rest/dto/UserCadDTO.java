package br.com.dev.gestaousuarioteste.rest.dto;

import lombok.*;

@Getter@Setter
@AllArgsConstructor
@NoArgsConstructor
@Generated
public class UserCadDTO {

    private String username;
    private String password;
    private String email;

}
