package br.com.dev.gestaousuarioteste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoUsuarioTesteApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoUsuarioTesteApplication.class, args);
	}

}
