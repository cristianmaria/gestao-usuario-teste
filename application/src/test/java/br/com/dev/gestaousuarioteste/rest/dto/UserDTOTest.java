package br.com.dev.gestaousuarioteste.rest.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDTOTest {

    @Test
    void equalsTest() {
        assertEquals(generateUser(), generateUser());
    }

    @Test
    void hashCodeTest() {
        assertEquals(generateUser().hashCode(), generateUser().hashCode());
    }

    @Test
    void toStringTest() {
        assertEquals(generateUser().toString(), generateUser().toString());
    }

    private UserDTO generateUser(){
        UserDTO user = new UserDTO();
        user.setId(1L);
        user.setUsername("teste1");
        user.setPassword("1233412");
        user.setEmail("teste1@gmail");
        return user;
    }
}