package br.com.dev.gestaousuarioteste.rest.mapper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserCadDTOMapperTest {

    @Test
    void toUserTest_null() {
        Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            UserCadDTOMapper.toUser(null);
        });
        assertEquals("Objeto userCadDTO nao pode ser nulo", exception.getMessage());
    }
}