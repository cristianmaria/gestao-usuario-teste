package br.com.dev.gestaousuarioteste.rest.controller;

import br.com.dev.gestaousuarioteste.core.domain.User;
import br.com.dev.gestaousuarioteste.core.service.UserService;
import br.com.dev.gestaousuarioteste.rest.dto.UserCadDTO;
import br.com.dev.gestaousuarioteste.rest.dto.UserDTO;
import br.com.dev.gestaousuarioteste.rest.mapper.UserCadDTOMapper;
import br.com.dev.gestaousuarioteste.rest.mapper.UserDTOMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveUserTest() {

        given(this.userService.saveUser(UserCadDTOMapper.toUser(generateUserCadDTO()))).willReturn(UserDTOMapper.toUser(generateUserDTO1()));

        UserDTO usersResult = userController.saveUser(generateUserCadDTO());
        assertEquals(generateUserDTO1(), usersResult);

    }

    @Test
    void getListUsersTest() {
        List<User> users = new ArrayList<>();
        users.add(UserDTOMapper.toUser(generateUserDTO1()));
        users.add(UserDTOMapper.toUser(generateUserDTO2()));

        given(this.userService.listUsers()).willReturn(users);

        List<UserDTO> usersResult = userController.getListUsers();
        assertEquals(generateUserDTO1(), usersResult.get(0));
        assertEquals(generateUserDTO2(), usersResult.get(1));
    }

    @Test
    void updateUserTest() {
        UserDTO userDTO = generateUserDTO1();
        User user = UserDTOMapper.toUser(userDTO);

        given(this.userService.updateUser(user)).willReturn(user);

        UserDTO usersResult = userController.updateUser(userDTO);
        assertEquals(userDTO, usersResult);

    }

    private UserCadDTO generateUserCadDTO(){
        UserCadDTO user = new UserCadDTO();
        user.setUsername("teste1");
        user.setPassword("1233412");
        user.setEmail("teste1@gmail");
        return user;
    }

    private UserDTO generateUserDTO1(){
        UserDTO user = new UserDTO();
        user.setId(1L);
        user.setUsername("teste1");
        user.setPassword("1233412");
        user.setEmail("teste1@gmail");
        return user;
    }

    private UserDTO generateUserDTO2(){
        UserDTO user = new UserDTO();
        user.setId(2L);
        user.setUsername("teste2");
        user.setPassword("87565432");
        user.setEmail("teste2@gmail");
        return user;
    }

}