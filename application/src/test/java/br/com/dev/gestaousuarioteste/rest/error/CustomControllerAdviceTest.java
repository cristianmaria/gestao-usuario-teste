package br.com.dev.gestaousuarioteste.rest.error;

import br.com.dev.gestaousuarioteste.core.exceptions.BusinessRuleException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CustomControllerAdviceTest {

    CustomControllerAdvice customControllerAdvice = new CustomControllerAdvice();

    @Test
    void handleNullPointerExceptionsTest(){
        String testeMensagemException = "Teste NullPointerException";
        Exception exception = new NullPointerException(testeMensagemException);

        ResponseEntity<ErrorResponse> response = customControllerAdvice.handleNullPointerExceptions(exception);
        ErrorResponse resultResponse = response.getBody();

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND, testeMensagemException);

        Assertions.assertEquals(404, response.getStatusCode().value());
        Assertions.assertEquals(errorResponse.toString(), resultResponse.toString());
    }

    @Test
    void handleBusinessRuleExceptionTest(){
        String testeMensagemException = "Teste BusinessRuleException";
        Exception exception = new BusinessRuleException(testeMensagemException);

        ResponseEntity<ErrorResponse> response = customControllerAdvice.handleBusinessRuleException(exception);
        ErrorResponse resultResponse = response.getBody();

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, testeMensagemException);

        Assertions.assertEquals(400, response.getStatusCode().value());
        Assertions.assertEquals(errorResponse.toString(), resultResponse.toString());
    }

    @Test
    void handleExceptionsTest(){
        String testeMensagemException = "Teste Exception";
        Exception exception = new Exception(testeMensagemException);

        ResponseEntity<ErrorResponse> response = customControllerAdvice.handleExceptions(exception);
        ErrorResponse resultResponse = response.getBody();

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, testeMensagemException);

        Assertions.assertEquals(500, response.getStatusCode().value());
        Assertions.assertEquals(testeMensagemException, resultResponse.getMessage());
    }

}
