package br.com.dev.gestaousuarioteste.rest.mapper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDTOMapperTest {

    @Test
    void toUserTest_null() {
        Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            UserDTOMapper.toUser(null);
        });
        assertEquals("Objeto userDTO nao pode ser nulo", exception.getMessage());
    }

    @Test
    void toUserDTOTest_null() {
        Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            UserDTOMapper.toUserDTO(null);
        });
        assertEquals("Objeto user nao pode ser nulo", exception.getMessage());
    }

    @Test
    void toListUserDTO_null() {
        Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            UserDTOMapper.toListUserDTO(null);
        });
        assertEquals("Objeto lista de user nao pode ser nulo", exception.getMessage());
    }

}